package capitol1;

public class Granja {
	private int numGossos;
	private int topGossos;
	private Gos[] gossos;

	public Granja() {
		topGossos = 100;
		gossos = new Gos[topGossos];
		numGossos = 0;

	}

	public Granja(int capacitat) {
		if (capacitat > 0 && capacitat <= 100) {
			topGossos = capacitat;
		} else {
			topGossos = 100;
		}
		gossos = new Gos[topGossos];
	}

	public int getNumGossos() {
		return numGossos;
	}

	public int getTopGossos() {
		return topGossos;
	}

	public Gos[] getGossos() {
		return gossos;
	}

	public int afegir(Gos perro) {
		if (numGossos < topGossos) {
			gossos[numGossos] = perro;
			numGossos++;
			return numGossos;
		} else {
			return -1;
		}
	}

	public String toString() {
		String qualitats = "La granja té una capacitat de " + topGossos + " gossos on hi ha " + numGossos + " gossos";
		return qualitats;
	}

	public void visualitzar() {
		System.out.println(this.toString());
		
		for (int i = 0; i<topGossos;i++) {
			if(gossos[i]!=null) {
			gossos[i].visualitzar();
			}
			
		}
	}

	public void visualitzarVius() {
		System.out.println(this.toString());
		
		for (int i = 0; i<topGossos;i++) {
			if (gossos[i].getEstat() == GosEstat.VIU) {
				if(gossos[i] != null) {
				gossos[i].visualitzar();
				}
			}
			
		}
	}

	static Granja generarGranja() {
		Gos gos1 = new Gos();
		Gos gos2 = new Gos("perro");
		Gos gos3 = new Gos('M', 8, "OtroPerro", 20);
		gos3.setEdat(40);

		Granja granja1 = new Granja();

		granja1.afegir(gos1);
		granja1.afegir(gos2);
		granja1.afegir(gos3);

		return granja1;
	}

	public Gos obtenirGos(int pos) {
		if (pos >= 0 && pos < gossos.length) {
			return gossos[pos];
		} else {
			return null;
		}
	}
}
