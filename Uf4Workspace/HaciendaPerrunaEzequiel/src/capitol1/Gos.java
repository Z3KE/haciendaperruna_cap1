package capitol1;

public class Gos {
	private int edat;
	private String nom;
	private int fills;
	private char sexe;
	private static int numGos = 0;
	private Raca racaGos;
	private GosEstat estat = GosEstat.VIU;

	public Gos(char s, int f, String n, int e, Raca r) {
		this(s, f, n, e);
		racaGos = r;
	}

	public Gos() {
		sexe = 'M';
		fills = 0;
		nom = "SenseNom";
		edat = 4;
		numGos++;
	}

	public Gos(char s, int f, String n, int e) {
		if(comprovaSexe(s)) {
			sexe = s;
		}else {
			sexe = 'F';
		}
		
		fills = f;
		nom = n;
		edat = e;
		numGos++;
	}

	public Gos(String nomenclatura) {
		sexe = 'M';
		fills = 0;
		nom = nomenclatura;
		edat = 4;
		numGos++;
	}

	public Gos(Gos copia) {
		sexe = copia.sexe;
		fills = copia.fills;
		nom = copia.nom;
		edat = copia.edat;
		numGos++;
	}

	// Accesors
	public void setEdat(int anys) {
		edat = anys;
		if (racaGos != null) {
			if (edat > racaGos.getVida()) {
				this.estat = GosEstat.MORT;
			}
		} else {
			if (edat > 10) {
				this.estat = GosEstat.MORT;
			}
		}
	}

	public void setNom(String nomenclatura) {
		nom = nomenclatura;
	}

	public void setFills(int descendencia) {
		fills = descendencia;
	}

	public void setSexe(char generoBinario) {
		boolean correcte=comprovaSexe(generoBinario);
		if(correcte) {
		sexe = generoBinario;
		}else {
			sexe= 'F';
		}
	}
	
	static private boolean comprovaSexe(char s) {
		if(s=='F'||s=='M') {
			return true;
		}else {
			return false;
		}
	}
	
	public int getEdat() {
		return edat;
	}

	public String getNom() {
		return nom;
	}

	public int getFills() {
		return fills;
	}

	public char getSexe() {
		return sexe;
	}

	public void clonar(Gos dolly) {
		this.sexe = dolly.sexe;
		this.fills = dolly.fills;
		this.nom = dolly.nom;
		this.edat = dolly.edat;
	}

	public static int getQuantitatGossos() {
		return (numGos);
	}

	public Raca getRaca() {
		return racaGos;
	}

	public GosEstat getEstat() {
		return estat;
	}

	public void setRaca(Raca r) {
		racaGos = r;
	}

	public void setEstat(GosEstat muertoVivo) {
		estat = muertoVivo;
	}
	// Mètodes

	public void borda() {
		System.out.println("guau, guau");
	}

	public String toString() {
		String propietats = "El nom del gos és " + nom + ", el seu sexe és " + sexe + ", la seva edat és de " + edat
				+ " anys, té " + fills + " fills i el se estat és :" + this.getEstat() ;
		if (racaGos != null) {
			propietats = propietats + " " + racaGos.toString();
		}
		return propietats;
	}

	public void visualitzar() {
		System.out.println(this.toString());
	}

	public Gos aparellar(Gos g) {
		if ((this.getEstat() == GosEstat.VIU && g.getEstat() == GosEstat.VIU)
				&& (this.getEdat() >= 2 && this.getEdat() <= 10 && g.getEdat() >= 2 && g.getEdat() <= 10)
				&& (this.getSexe() == 'M' && g.getSexe() == 'F' && g.getFills() <= 3
						|| this.getSexe() == 'F' && g.getSexe() == 'M' && this.getFills() <= 3)) {

			if (this.getSexe() == 'M') {
				if (g.getRaca().getMida() == GosMida.MITJA && this.getRaca().getMida() == GosMida.PETIT
						|| g.getRaca().getMida() == GosMida.GRAN && this.getRaca().getMida() == GosMida.PETIT
						|| g.getRaca().getMida() == GosMida.GRAN && this.getRaca().getMida() == GosMida.MITJA
						|| this.getRaca().getMida() == g.getRaca().getMida()) {
					// generar Hijo this = Mascle g =Femmella
					int max = 2;
					int min = 1;
					int rang = max - min + 1;
					int random = (int) (Math.random() * rang) + min;
					char sex;
					if (random == 1) {
						sex = 'M';
					} else {
						sex = 'F';
					}

					String nombre;
					if (sex == 'M') {
						nombre = "fill de " + this.getNom();
					} else {
						nombre = "fill de " + g.getNom();
					}

					if (this.getRaca().getDominant() == true && g.getRaca().getDominant() == true
							|| this.getRaca().getDominant() == false && g.getRaca().getDominant() == true
							|| this.getRaca().getDominant() == false && g.getRaca().getDominant() == false) {
						// mare
						Gos fill = new Gos(sex, 0, nombre, 0, g.getRaca());
						this.setFills(this.getFills()+1);
						g.setFills(g.getFills()+1);
						return fill;
					} else {
						// pare
						Gos fill = new Gos(sex, 0, nombre, 0, this.getRaca());
						this.setFills(this.getFills()+1);
						g.setFills(g.getFills()+1);
						return fill;
					}
				}
			} else {
				if (this.getRaca().getMida() == GosMida.MITJA && g.getRaca().getMida() == GosMida.PETIT
						|| this.getRaca().getMida() == GosMida.GRAN && g.getRaca().getMida() == GosMida.PETIT
						|| this.getRaca().getMida() == GosMida.GRAN && g.getRaca().getMida() == GosMida.MITJA
						|| this.getRaca().getMida() == g.getRaca().getMida()) {
					// generar hijo this this = F g = M

					int max = 2;
					int min = 1;
					int rang = max - min + 1;
					int random = (int) (Math.random() * rang) + min;
					char sex;
					if (random == 1) {
						sex = 'M';
					} else {
						sex = 'F';
					}

					String nombre;
					if (sex == 'M') {
						nombre = "fill de " + g.getNom();
					} else {
						nombre = "fill de " + this.getNom();
					}
					if (g.getRaca().getDominant() == true && this.getRaca().getDominant() == true
							|| g.getRaca().getDominant() == false && this.getRaca().getDominant() == true
							|| g.getRaca().getDominant() == false && this.getRaca().getDominant() == false) {
						// mare
						Gos fill = new Gos(sex, 0, nombre, 0, this.getRaca());
						this.setFills(this.getFills()+1);
						g.setFills(g.getFills()+1);
						return fill;
					} else {
						// pare
						Gos fill = new Gos(sex, 0, nombre, 0, g.getRaca());
						this.setFills(this.getFills()+1);
						g.setFills(g.getFills()+1);
						return fill;
					}
				}
			}

		}
		return null;
	}
}