package capitol1;

public class Raca {
	private String nomRaca;
	private GosMida mida;
	private int tempsVida;
	private boolean dominant;
	
	public Raca(String nom, GosMida gt,int t) {
		this(nom,gt);
		tempsVida =t;
		
	}
	
	public Raca(String nom, GosMida gt) {
		nomRaca =nom;
		mida= gt;
		tempsVida =10;
		dominant =false;
	}
	
	public void setNomRaca(String NRaca) {
		nomRaca=NRaca;
	}
	
	public void setMida(GosMida m) {
		mida =m;
	}
	
	public void setVida(int temps) {
		tempsVida = temps;
	}
	
	public void setDominant(boolean domination) {
		dominant =domination;
	}
	
	
	public String getNomRaca() {
		return nomRaca;
	}
	
	public GosMida getMida() {
		return mida;
	}
	
	public int getVida() {
		return tempsVida;
	}
	
	public boolean getDominant() {
		return dominant;
	}
	
	public String toString() {
		String propietats;
		if (dominant) {
		propietats = "La raça del gos és " + nomRaca + ", és un gos " + mida + ", el seu temps estimat de vida és de " + tempsVida
				+ " anys i és dominant";
		}else {
			propietats = "La raça del gos és " + nomRaca + ", és un gos " + mida + ", el seu temps estimat de vida és de " + tempsVida
					+ " anys i no és dominant";
		}
		return propietats;
	}
}
