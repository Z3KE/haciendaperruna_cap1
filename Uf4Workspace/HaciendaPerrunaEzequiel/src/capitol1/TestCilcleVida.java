package capitol1;

import java.util.Scanner;

public class TestCilcleVida {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		boolean anysBe = false;
		int temps = 0;
		while (!anysBe) {
			System.out.println("Quants anys vols controlar?");
			temps = reader.nextInt();
			if (temps > 0 && temps <= 20) {
				anysBe = true;
			}
		}

		Raca raca1 = new Raca("Chiwawa", GosMida.PETIT, 13);
		Raca raca2 = new Raca("Terrier", GosMida.PETIT, 12);
		Raca raca3 = new Raca("Husky", GosMida.GRAN, 15);
		Raca raca4 = new Raca("Pastor alemán", GosMida.GRAN, 15);
		Raca raca5 = new Raca("Schnoodle", GosMida.MITJA, 12);
		Raca raca6 = new Raca("Bóxer", GosMida.MITJA, 14);
		Raca raca7 = new Raca("Mal-shi", GosMida.PETIT, 13);
		Raca raca8 = new Raca("Puggle", GosMida.PETIT, 12);
		Raca raca9 = new Raca("Pomsky", GosMida.MITJA, 14);
		Raca raca10 = new Raca("Harrier", GosMida.MITJA, 10);

		Gos gos1 = new Gos('M', 2, "Perrochiquito1", 3, raca1);
		Gos gos2 = new Gos('F', 3, "Perrochiquito2", 6, raca2);
		Gos gos3 = new Gos('M', 0, "Popo", 3, raca3);
		Gos gos4 = new Gos('F', 1, "Pipo", 4, raca4);
		Gos gos5 = new Gos('M', 3, "Pepe", 6, raca5);
		Gos gos6 = new Gos('F', 2, "Pepa", 5, raca6);
		Gos gos7 = new Gos('M', 2, "Paco", 3, raca7);
		Gos gos8 = new Gos('M', 3, "Pepo", 7, raca8);
		Gos gos9 = new Gos('M', 3, "Licmo", 6, raca9);
		Gos gos10 = new Gos('F', 2, "Licma", 5, raca10);

		Granja granja1 = new Granja(100);

		granja1.afegir(gos1);
		granja1.afegir(gos2);
		granja1.afegir(gos3);
		granja1.afegir(gos4);
		granja1.afegir(gos5);
		granja1.afegir(gos6);
		granja1.afegir(gos7);
		granja1.afegir(gos8);
		granja1.afegir(gos9);
		granja1.afegir(gos10);

		for (int i = 0; i < temps; i++) {
			int numGossos= granja1.getNumGossos();
			Gos[] gossosGranja = granja1.getGossos();
			
			
			int j = 0;
			while (j<numGossos && gossosGranja[j+1]!= null) {
				
				Gos fill = gossosGranja[j].aparellar(gossosGranja[j + 1]);
				if (fill != null) {
					granja1.afegir(fill);
					fill.visualitzar();
					gossosGranja[j].setEdat(gossosGranja[j].getEdat()+1);
					gossosGranja[j+1].setEdat(gossosGranja[j+1].getEdat()+1);
					j= j+2;
				}else {
					Gos aux= gossosGranja[j];
					gossosGranja[j]= gossosGranja[j+1];
					gossosGranja[j+1]= aux;
					gossosGranja[j].setEdat(gossosGranja[j].getEdat()+1);
					j++;
				}
			}
			System.out.println();
			granja1.visualitzar();
		}
		System.out.println(granja1.getNumGossos());
	}
}
