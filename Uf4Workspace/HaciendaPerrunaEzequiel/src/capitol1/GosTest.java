package capitol1;

import java.util.Scanner;

public class GosTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner reader = new Scanner(System.in);

		Raca racaP1 = new Raca("Chiwawa", GosMida.PETIT, 13);

		Raca racaP2 = new Raca("Terrier", GosMida.PETIT, 12);

		Raca racaG3 = new Raca("Husky", GosMida.GRAN, 15);

		Raca racaG4 = new Raca("Pastor alemán", GosMida.GRAN, 15);

		Raca racaM5 = new Raca("Schnoodle", GosMida.MITJA, 12);

		Raca racaM6 = new Raca("Bóxer", GosMida.MITJA, 14);

		Raca racaP7 = new Raca("Mal-shi", GosMida.PETIT, 13);

		Raca racaP8 = new Raca("Puggle", GosMida.PETIT, 12);

		Raca racaM9 = new Raca("Pomsky", GosMida.MITJA, 14);

		Raca racaM10 = new Raca("Harrier", GosMida.MITJA, 10);

		Raca racaG11 = new Raca("Braco francés", GosMida.GRAN, 13);

		Raca racaG12 = new Raca("Eurasier", GosMida.GRAN, 11);

		Gos estandard = new Gos();

		Gos pipo = new Gos("Pipo");

		Gos pipoCopia = new Gos(pipo);

		System.out.println("Com es diu el gos? ");
		String nom = reader.next();

		System.out.println("Quants anys té " + nom + "?");
		int anys = reader.nextInt();

		System.out.println("De quin sexe és " + nom + "?");
		char sexe = reader.next().charAt(0);

		System.out.println("Quans fills té " + nom + "?");
		int fills = reader.nextInt();

		Gos gosParametre = new Gos(sexe, fills, nom, anys);

		estandard.visualitzar();
		estandard.borda();

		pipo.visualitzar();
		pipo.borda();

		pipoCopia.visualitzar();
		pipoCopia.borda();

		pipo.clonar(gosParametre);
		pipo.visualitzar();
		pipo.borda();
		pipo.visualitzar();

		Gos gosP1 = new Gos('M', 2, "Perrochiquito1", 3, racaP1);
		Gos gosP2 = new Gos('F', 3, "Perrochiquito2", 6, racaP2);
		Gos gosG3 = new Gos('M', 0, "Popo", 3, racaG3);
		Gos gosG4 = new Gos('F', 1, "Pipo", 4, racaG4);
		Gos gosM5 = new Gos('M', 3, "Pepe", 6, racaM5);
		Gos gosM6 = new Gos('F', 2, "Pepa", 5, racaM6);

		Gos gosP7 = new Gos('M', 2, "Paco", 3, racaP7);
		Gos gosP8 = new Gos('M', 3, "Pepo", 7, racaP8);
		Gos gosM9 = new Gos('M', 3, "Licmo", 6, racaM9);
		Gos gosM10 = new Gos('F', 2, "Licma", 5, racaM10);
		Gos gosG11 = new Gos('F', 0, "Manoli", 3, racaG11);
		Gos gosG12 = new Gos('F', 1, "Manola", 4, racaG12);
		System.out.println("El nombre total de gossos és: " + Gos.getQuantitatGossos());

		Granja granja1 = new Granja(6);

		granja1.afegir(gosP1);
		granja1.afegir(gosM5);
		granja1.afegir(gosG3);
		granja1.afegir(gosP7);
		granja1.afegir(gosP8);
		granja1.afegir(gosM9);

		Granja granja2 = new Granja(6);

		granja2.afegir(gosP2);
		granja2.afegir(gosM6);
		granja2.afegir(gosG4);
		granja2.afegir(gosM10);
		granja2.afegir(gosG11);
		granja2.afegir(gosG12);

		Granja granja3 = new Granja(6);
		
		for (int i = 0; i < 6; i++) {
			if(granja1.obtenirGos(i).aparellar(granja2.obtenirGos(i))!=null) {
				granja1.obtenirGos(i).setFills(granja1.obtenirGos(i).getFills()-1);
				granja2.obtenirGos(i).setFills(granja2.obtenirGos(i).getFills()-1);
			granja3.afegir(granja1.obtenirGos(i).aparellar(granja2.obtenirGos(i)));
			}
		}
		
		granja1.visualitzar();
		granja2.visualitzar();
		granja3.visualitzar();
		granja1.visualitzarVius();
		reader.close();

	}

}
